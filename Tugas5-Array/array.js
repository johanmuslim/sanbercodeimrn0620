// Soal No. 1 (Range)
console.log("--------------------");
console.log("Soal No. 1 (Range)");
console.log("--------------------");

function range(startNum, finishNum) {
    if (!startNum || !finishNum) {
        return -1;
    } else {
        let rangeArray = [];
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i++) {
                rangeArray.push(i);
            }
        } else {
            for (let i = startNum; i >= finishNum; i--) {
                rangeArray.push(i);
            }
        }
        return rangeArray;
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
console.log("-----------------------");
console.log("Soal No. 2 (Range with Step)");
console.log("-----------------------");

function rangeWithStep(startNum, finishNum, step) {
    if (!startNum || !finishNum) {
        return -1;
    } else {
        let rangeArray = [];
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += step) {
                rangeArray.push(i);
            }
        } else {
            for (let i = startNum; i >= finishNum; i -= step) {
                rangeArray.push(i);
            }
        }
        return rangeArray;
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal No. 3 (Sum of Range)
console.log("-----------------------"); 
console.log("Soal No. 3 (Sum of Range)");
console.log("-----------------------");

function sum(startNum, finishNum, step = 1) {
    if (!startNum) {
        return 0;
    } else if (!finishNum) {
        return startNum;
    } else {
        var rangeArray = [];
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += step) {
                rangeArray.push(i);
            }
        } else {
            for (let i = startNum; i >= finishNum; i -= step) {
                rangeArray.push(i);
            }
        }
        for (var j = 0, sum = 0; j < rangeArray.length; j++) {
            sum += rangeArray[j];
        }
        return sum;
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
console.log("-----------------------");
console.log("Soal No. 4 (Array Multidimensi)");
console.log("-----------------------");

function dataHandling(input) {
    var label = ["Nomor ID: ", "Nama Lengkap: ", "TTL: ", "Hobi: "];
    for (let i = 0; i < input.length; i++) {
        input[i].splice(2, 2, input[i][2] + " " + input[i][3]); // gabung data TTL
        var contentLength = input[i].length;
        for (let j = 0; j < contentLength; j++) {
            console.log(label[j] + input[i][j]);
        }
        console.log("\n"); 
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]; 
dataHandling(input);

// Soal No. 5 (Balik Kata)
console.log("-----------------------");
console.log("Soal No. 5 (Balik Kata)");
console.log("-----------------------");

function balikKata(malang) {
    var ngalam = "";
    for (let i = malang.length - 1; i >= 0; i--) {
        ngalam = ngalam + malang[i];
    }
    return ngalam;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)
console.log("-----------------------");
console.log("Soal No. 6 (Metode Array)");
console.log("-----------------------");

function dataHandling2 (input) {
    input.splice(1, 4, input[1] + " Elsharawy", "Provinsi " + input[2], input[3], "Pria", "SMA Internasional Metro");
    console.log(input);
    var tanggal = input[3].split("/");
    switch (tanggal[1]) {
        case "01": { var namaBulan = "Januari"; break; }
        case "02": { var namaBulan = "Februari"; break; }
        case "03": { var namaBulan = "Maret"; break; }
        case "04": { var namaBulan = "April"; break; }
        case "05": { var namaBulan = "Mei"; break; }
        case "06": { var namaBulan = "Juni"; break; }
        case "07": { var namaBulan = "Juli"; break; }
        case "08": { var namaBulan = "Agustus"; break; }
        case "09": { var namaBulan = "September"; break; }
        case "10": { var namaBulan = "Oktober"; break; }
        case "11": { var namaBulan = "November"; break; }
        case "12": { var namaBulan = "Desember"; break; }    
    }
    console.log(namaBulan);
    var sortDesc = tanggal.sort(function (value1, value2) { return value2 - value1 } );
    console.log(sortDesc);
    var tanggal = input[3].split("/");
    console.log(tanggal.join("-"));
    console.log(input[1].slice(0, 15));
}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
