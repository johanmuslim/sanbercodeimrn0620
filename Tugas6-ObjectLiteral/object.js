// Soal No. 1 (Array to Object)
var arr = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini 
    if (arr.length === 0) {
        console.log("")
    } else {
        for (i = 0; i < arr.length; i++) {
            var data = {}
            data.firstName = arr[i][0]
            data.lastName = arr[i][1]
            data.gender = arr[i][2]
            if (!arr[i][3] || arr[i][3] > thisYear) {
                data.age = "Invalid birth year"
            } else {
                data.age = thisYear - arr[i][3]
            }
            console.log(i+1 + ". " + data.firstName + " " + data.lastName + ":")
            console.log(data)
        }
    }
}
arrayToObject(arr)

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var changeMoney = money
        var listPurchased = []
        while (changeMoney >= 50000) {
            if (changeMoney >= 1500000) {
                listPurchased.push("Sepatu Stacattu")
                changeMoney -= 1500000
            } else if (changeMoney >= 500000) {
                listPurchased.push("Baju Zoro")
                changeMoney -= 500000
            } else if (changeMoney >= 250000) {
                listPurchased.push("Baju H&N")
                changeMoney -= 250000
            } else if (changeMoney >= 175000) {
                listPurchased.push("Sweater Uniklooh")
                changeMoney -= 175000
            } else if (changeMoney >= 50000) {
                listPurchased.push("Casing handphone")
                changeMoney -= 50000
                break
            } 
        }
        var shoppingData = {
            memberId: memberId,
            money: money,
            listPurchased: listPurchased,
            changeMoney: changeMoney
        }
        return shoppingData
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    if (arrPenumpang.length === 0) {
        return []
    } else {
        var dataPenumpang = []
        for (i = 0; i < arrPenumpang.length; i++) {
            var listPenumpang = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
            }
            dataPenumpang.push(listPenumpang)
        }
        return dataPenumpang
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]