// No. 1 Looping While
var x = 1;
console.log('LOOPING PERTAMA');
while (x < 11) {
  console.log((x * 2) + ' - I Love Coding' );
  x ++;
}

console.log('LOOPING KEDUA');
while (x > 1) {
  x --;
  console.log((x * 2) + ' - I will become a mobile developer' );
}

// No. 2 Looping menggunakan for
console.log('OUTPUT');
for (var x = 1; x < 21; x ++) {
  if (x % 2 === 0) {
    console.log(x + ' - Berkualitas');
  } else {
    if (x % 3 === 0) {
      console.log(x + ' - I Love Coding');
    } else {
      console.log(x + ' - Santai');
    }
  }
}

// No. 3 Membuat Persegi Panjang
var width = 8;
var height = 4;
var pattern = '#';
for (var x = 1; x <= height; x ++) {
    console.log(pattern.repeat(width));
}

// No. 4 Membuat Tangga
var height = 7;
var pattern = '#';
for (var x = 1; x <= height; x ++) {
  console.log(pattern.repeat(x));
}

// No. 5 Membuat Papan Catur
var width = 8;
var height = 8;
var pattern1 = ' ';
var pattern2 = '#';
for (var x = 1; x <= height; x ++) {
    if (x % 2 === 0) {
        console.log((pattern2+pattern1).repeat(width/2));
    } else {
      console.log((pattern1+pattern2).repeat(width/2));
    }
}