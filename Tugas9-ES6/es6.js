// 1. Mengubah fungsi menjadi fungsi arrow
const goldenFunction = () => `this is golden!!`
const golden = console.log(goldenFunction ())

// 2. Sederhanakan menjadi Object literal di ES6
const literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: `${firstName} ${lastName}`
    }
}
const newFunction = console.log(literal("William", "Imoh").fullName)

// 3. Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation)

// 4. Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

// 5. Template Literals
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 