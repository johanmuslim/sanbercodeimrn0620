var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0;
function startRead(times) {
    if (i < books.length) {
        readBooksPromise(times,books[i])
        .then(function (result) {
            startRead(result)
        })
        .catch(function (error) {
            console.log(error)
        })
    }
    i++
}
startRead(10000)