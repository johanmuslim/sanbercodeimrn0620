// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
const mulaiBaca = (index, waktu) => {
    index > books.length - 1 ? '' :
    readBooks(waktu, books[index], waktu => 
        index + mulaiBaca(index + 1, waktu)
    )
}

mulaiBaca(0, 10000)