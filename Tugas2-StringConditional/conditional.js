// if-else

var nama = "Juned";
var peran = "Penyihir";
var errorNama = "Nama harus diisi!";
var halo = "Halo ";
var errorPeran = halo + nama + ", Pilih peranmu untuk memulai game!";
var errorPeranAlt = halo + nama + ", sekarang diem dulu ya karena peranmu unfaedah!";
var welcome = "Selamat datang di Dunia Werewolf, " + nama;
var peranPenyihir = halo + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!";
var peranGuard = halo + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.";
var peranWerewolf = halo + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!";

if (nama == '') { 
    console.log(errorNama);
} else {
    if (peran == '') { 
        console.log(errorPeran);
    } else {
        console.log(welcome);
        if (peran == 'Penyihir') { 
            console.log(peranPenyihir);
        } else if (peran == 'Guard') { 
            console.log(peranGuard);
        } else if (peran == 'Werewolf') { 
            console.log(peranWerewolf);
        } else { 
            console.log(errorPeranAlt);
        }
    }
}

// switch case

var hari = 16; 
var bulan = 6; 
var tahun = 2020;

switch (bulan) {
    case 1: { var bulan = " Januari "; break; }
    case 2: { var bulan = " Februari "; break; }
    case 3: { var bulan = " Maret "; break; }
    case 4: { var bulan = " April "; break; }
    case 5: { var bulan = " Mei "; break; }
    case 6: { var bulan = " Juni "; break; }
    case 7: { var bulan = " Juli "; break; }
    case 8: { var bulan = " Agustus "; break; }
    case 9: { var bulan = " September "; break; }
    case 10: { var bulan = " Oktober "; break; }
    case 11: { var bulan = " November "; break; }
    case 12: { var bulan = " Desember "; break; }    
    default: { var bulan = " salah_bulan "; }
}

console.log(hari + bulan + tahun);